package com.fintech.controller;

import com.fintech.service.City;
import com.fintech.service.RouteService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.ModelAndView;

import java.util.Arrays;
import java.util.List;

import static com.fintech.service.City.EMPTY;
import static com.fintech.service.City.getByName;

/**
 * Created by alex on 05.04.17.
 */
@Controller

public class RouteController {

    @Autowired
    private RouteService routeService;

    @ModelAttribute("departure")
    public City populateDeparture() {
        return EMPTY;
    }

    @ModelAttribute("arrival")
    public City populateArrival() {
        return EMPTY;
    }

    @ModelAttribute("departures")
    public City[] populateDepartures() {
        return routeService.getPossiblePointsDepartures();
    }

    @ModelAttribute("arrivals")
    public List<City> populateDepartures(ModelAndView modelAndView) {
        return Arrays.asList(EMPTY);
    }

    @RequestMapping(value = "/routes")
    public void init(Model model) {
    }

    @RequestMapping(value = "/routes/departure")
    public String getArrivals(@RequestParam String departure, Model model) {
        model.addAttribute("departure", getByName(departure));
        model.addAttribute("arrivals", routeService.getPossiblePointsArrivals(getByName(departure)));

        return "routes::arrivalsList";
    }

    @RequestMapping(value = "/routes/price/{departure}/{arrival}")
    public String getPrice(@PathVariable("departure") String departure, @PathVariable("arrival") String arrival, Model model) {
        model.addAttribute("price", routeService.calculatePrice(getByName(departure), getByName(arrival)));
        return "routes::price";
    }

}
