package com.fintech.service;

/**
 * Created by alex on 05.04.17.
 */
public enum City {
    KIEV(1, "Киев"),
    GITOMIR(2, "Житомир"),
    LVOV(3, "Львов"),
    KRAKOV(4, "Краков"),
    PRAGUE(5, "Прага"),
    NERNBERNG(6, "Нюрнберг"),
    MUNHEN(7, "Мюнхен"),
    FRANKFURT(8, "Франкфурт"),
    ODESSA(9, "Одесса"),
    UMAN(10, "Умань"),
    VINNITSA(11, "Винница"),
    CHMELNITSKI(12, "Хмельнитский"),
    TERNOPIL(13, "Тернополь"),
    BERLIN(14, "Берлин"),
    GAMBURG(15, "Гамбург"),

    EMPTY(0, "Не выбран");

    private int id;
    private String name;

    City(int id, String name) {
        this.id = id;
        this.name = name;
    }

    public static City getByName(String name){
        for (City city: values()){
            if (city.getName().equals(name)) return city;
        }
        return null;
    }

    public int getId() {
        return id;
    }

    public String getName() {
        return name;
    }
}
