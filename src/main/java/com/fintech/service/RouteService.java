package com.fintech.service;

import org.springframework.stereotype.Service;

import java.math.BigDecimal;
import java.util.*;

import static com.fintech.service.City.*;
import static java.util.Arrays.asList;
import static java.util.stream.Collectors.toCollection;

/**
 * Created by alex on 05.04.17.
 */
@Service
public class RouteService {

    private final List<City> ROUTE1 = asList(KIEV, GITOMIR, LVOV, KRAKOV, PRAGUE, NERNBERNG, MUNHEN, FRANKFURT);
    private final List<City> ROUTE2 = asList(KIEV, GITOMIR, LVOV, KRAKOV, BERLIN, GAMBURG);
    private final List<City> ROUTE3 = asList(ODESSA, UMAN, VINNITSA, CHMELNITSKI, TERNOPIL, LVOV, KRAKOV, BERLIN, GAMBURG);
    private final List<City> ROUTE4 = asList(ODESSA, UMAN, VINNITSA, CHMELNITSKI, TERNOPIL, LVOV, KRAKOV, PRAGUE, NERNBERNG, MUNHEN, FRANKFURT);
    private final List<List<City>> ROUTES = asList(ROUTE1, ROUTE2, ROUTE3, ROUTE4);
    private final Map<City, List<City>> POSSIBLE_ROUTES = createPossibleRoutes();
    private final BigDecimal ATOMIC_PRICE = new BigDecimal(50);

    public City[] getPossiblePointsDepartures() {
        return City.values();
    }

    public List<City> getPossiblePointsArrivals(City pointDeparture) {
        return POSSIBLE_ROUTES.get(pointDeparture);
    }

    public BigDecimal calculatePrice(City from, City to) {
        List<City> route = ROUTES.stream().filter(cities -> cities.contains(from) && cities.contains(to)).findFirst().get();
        List<City> subList = route.subList(route.indexOf(from), route.indexOf(to));
        return ATOMIC_PRICE.multiply(new BigDecimal(subList.size()));
    }

    private Map<City, List<City>> createPossibleRoutes() {
        Map<City, List<City>> cityListMap = new HashMap<>();
        for (City city : City.values()) {
            cityListMap.put(city, calculatePossibleDepartures(city));
        }
        return cityListMap;
    }

    private List<City> calculatePossibleDepartures(City city) {
        List<City> result = new ArrayList<>();
        ROUTES.stream().filter(cities -> cities.contains(city)).forEach(cities -> {
            List<City> subList = cities.subList(cities.indexOf(city) + 1, cities.size());
            subList.stream().filter(c -> !result.contains(c)).collect(toCollection(() -> result));
        });
        return result;
    }
}
