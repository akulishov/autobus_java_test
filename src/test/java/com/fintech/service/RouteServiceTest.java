package com.fintech.service;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.test.context.junit4.SpringRunner;


import java.math.BigDecimal;

import static com.fintech.service.City.*;
import static java.util.Arrays.asList;
import static java.util.Arrays.setAll;
import static org.junit.Assert.assertEquals;

/**
 * Created by alex on 05.04.17.
 */
@RunWith(SpringRunner.class)
public class RouteServiceTest {

    private RouteService routeService = new RouteService();


    @Test
    public void testGetPossiblePointsDepartures() {
        assertEquals(City.values(), routeService.getPossiblePointsDepartures());
    }

    @Test
    public void testGetPossiblePointsArrivals() {
        assertEquals(asList(LVOV, KRAKOV, PRAGUE, NERNBERNG, MUNHEN, FRANKFURT, BERLIN, GAMBURG),
                routeService.getPossiblePointsArrivals(GITOMIR));
        assertEquals(asList(GAMBURG), routeService.getPossiblePointsArrivals(BERLIN));
        assertEquals(asList(PRAGUE, NERNBERNG, MUNHEN, FRANKFURT, BERLIN, GAMBURG),
                routeService.getPossiblePointsArrivals(KRAKOV));
        assertEquals(asList(UMAN, VINNITSA, CHMELNITSKI, TERNOPIL, LVOV, KRAKOV, BERLIN, GAMBURG,
                PRAGUE, NERNBERNG, MUNHEN, FRANKFURT),
                routeService.getPossiblePointsArrivals(ODESSA));
    }

    @Test
    public void testCalculatePrice(){
        assertEquals(new BigDecimal(50), routeService.calculatePrice(KIEV, GITOMIR));
        assertEquals(new BigDecimal(350), routeService.calculatePrice(KIEV, FRANKFURT));
    }
}
